#!/bin/sh

export RUSTUP_HOME=/opt/rust/rustup
export CARGO_HOME=/opt/rust/cargo

. $CARGO_HOME/env

"$@"
