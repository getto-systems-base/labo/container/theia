FROM node:12.21.0-buster

ENV RUSTUP_HOME /opt/rust/rustup
ENV CARGO_HOME /opt/rust/cargo

RUN : && \
  : "install packages : 2021-02-02" && \
  apt-get update && \
  apt-get install -y \
    ripgrep \
    clang-format \
  && \
  : "setup work dir" && \
  mkdir /opt/theia && \
  chown 1000:1000 /opt/theia && \
  mkdir /opt/rust && \
  chown 1000:1000 /opt/rust && \
  : "cleanup apt caches" && \
  apt-get clean && \
  rm -rf /var/lib/apt/lists/* && \
  :

COPY entrypoint.sh /
COPY theia/package.json /opt/theia/package.json

WORKDIR /opt/theia

USER 1000

RUN : && \
  : "setup rust" && \
  sh -c "$(curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | cat)" -- -q -y && \
  : "build theia" && \
  yarn && \
  sed -i lib/index.html -e 's|</script>|</script><link rel="stylesheet" href="./custom.css">|' && \
  :

COPY theia/custom.css /opt/theia/lib/custom.css

ENTRYPOINT ["/entrypoint.sh"]
